/*
 * SPDX-FileCopyrightText: 2022-2023 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <driver/adc.h>
#include <time.h>
#include <math.h>
#include <nvs_flash.h>
#include <mqtt5_client.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "soc/soc_caps.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

#include "esp_adc/adc_oneshot.h"
#include "esp_adc/adc_cali.h"
#include "esp_adc/adc_cali_scheme.h"

#include "esp_wifi.h"
//#include "mqtt_client.h"


const static char *TAG = "SMART_WIRELESS_SENSOR";
static esp_netif_t *s_example_sta_netif = NULL;
/*---------------------------------------------------------------
        ADC General Macros
---------------------------------------------------------------*/
//ADC1 Channels
#if CONFIG_IDF_TARGET_ESP32
#define EXAMPLE_ADC1_CHAN0          ADC_CHANNEL_4
#else
#define EXAMPLE_ADC1_CHAN0          ADC_CHANNEL_2
#define EXAMPLE_ADC1_CHAN1          ADC_CHANNEL_3
#endif

#define ADC_BITS ADC_WIDTH_BIT_12
#define ADC_ATTEN ADC_ATTEN_DB_0

#if (SOC_ADC_PERIPH_NUM >= 2) && !CONFIG_IDF_TARGET_ESP32C3
/**
 * On ESP32C3, ADC2 is no longer supported, due to its HW limitation.
 * Search for errata on espressif website for more details.
 */
#endif

#define EXAMPLE_ADC_ATTEN           ADC_ATTEN_DB_11


#define MQTT_BROKER_URI "http://192.168.0.1"
#define MQTT_TOPIC "sensor/adc"

// Circular Buffer Configuration
#define BUFFER_SIZE 1200
#define NUM_CHUNKS 4  // Number of chunks in a set
#define CHUNK_SIZE (BUFFER_SIZE / NUM_CHUNKS)
static unsigned char adc_values[BUFFER_SIZE];
static uint32_t buffer_head = 0;
static SemaphoreHandle_t buffer_mutex;


//static esp_err_t mqtt_app_start(const char *broker_uri, const char *topic);

static void wifi_event_handler(void *ctx, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    switch (event_id) {
        case WIFI_EVENT_STA_START:

            esp_wifi_connect();
            break;
        case WIFI_EVENT_STA_CONNECTED:
            // Handle connected event
            break;
        case IP_EVENT_STA_GOT_IP:
            // Handle got IP event
            break;
        case WIFI_EVENT_STA_DISCONNECTED:
            // Handle disconnected event
            break;
        default:
            break;
    }
}


static void initialize_wifi() {


//    ESP_ERROR_CHECK(
//            esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    esp_netif_inherent_config_t esp_netif_config = ESP_NETIF_INHERENT_DEFAULT_WIFI_STA();
    esp_netif_config.if_desc = "SMART_WIRELESS_SENS";
    esp_netif_config.route_prio = 128;
    s_example_sta_netif = esp_netif_create_wifi(WIFI_IF_STA, &esp_netif_config);
    esp_wifi_set_default_wifi_sta_handlers();

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));


    ESP_ERROR_CHECK(esp_wifi_start());


    wifi_config_t wifi_config = {
            .sta = {
                    .ssid = "ZyXEL_6DD5",
                    .password = "CMHNKMC3QL",
            },
    };

    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    esp_wifi_connect();
}

static esp_mqtt_client_handle_t mqtt_client;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event) {
    // Handle MQTT events if needed
    return ESP_OK;
}

static void initialize_mqtt() {
    esp_mqtt_client_config_t mqtt_cfg = {
            .broker.address.uri = "mqtt://your_broker_address",
            .broker.address.transport = MQTT_TRANSPORT_OVER_TCP,
    };

//    esp_mqtt_client_register_event(mqtt_client, &mqtt_event_handler);
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(mqtt_client);
}

static uint32_t generate_simulated_adc_value() {
    static float t = 0.0f;
    float amplitude = 255.0f;  // Adjust the amplitude as needed
    float frequency = 1.0f;  // Adjust the frequency as needed
    uint32_t simulated_adc_value = (uint32_t) (amplitude * sin(2 * M_PI * frequency * t));

    // Increment time variable for the next iteration
    t += 1.0f / 120.0f;  // Assuming a 120Hz sampling rate

    return simulated_adc_value;
}

// Task to sample ADC
static void adc_task(void *pvParameter) {


    while (1) {



        // unsigned char adc_value = adc1_get_raw(EXAMPLE_ADC1_CHAN0);
        unsigned char adc_value = generate_simulated_adc_value();

        ESP_LOGI(TAG, "ADC Value: %u", adc_value);


        // Investigate if any ADC content is lost when semaphore is busy,
        // Consider temporary buffer to append if so.
        if (xSemaphoreTake(buffer_mutex, portMAX_DELAY)) {
            adc_values[buffer_head] = adc_value;
            buffer_head = (buffer_head + 1) % BUFFER_SIZE;

            xSemaphoreGive(buffer_mutex);
        }

        vTaskDelay(pdMS_TO_TICKS(10));
    }
}

static void mqtt_task(void *pvParameter) {
    while (1) {
        // Add your MQTT handling code here
        // ...
        if (xSemaphoreTake(buffer_mutex, portMAX_DELAY)) {

            if (CHUNK_SIZE <= buffer_head) {
                // Process the chunk
                unsigned char chunk[CHUNK_SIZE];
                for (int i = 0; i < CHUNK_SIZE; i++) {
                    chunk[i] = adc_values[i];
                }


                buffer_head = 0;
                xSemaphoreGive(buffer_mutex);

                // Rudimentary, keep track of time - update from MQTT while keeping track of internal time.
                time_t now;
                time(&now);

                //  MQTT handling code here for the chunk
                ESP_LOGI(TAG, "MQTT Task - Processed Chunk:");
                char payload[CHUNK_SIZE];
                int payload_length = snprintf(payload, sizeof(payload),
                                              "{ \"timestamp\": %lld, \"datalen\": %u \"data\": [", now,
                                              sizeof(payload));

                // Append all values in the chunk
                for (int i = 0; i < CHUNK_SIZE; i++) {
                    payload_length += snprintf(payload + payload_length, sizeof(payload) - payload_length, " %u,",
                                               chunk[i]);
                }

                if (payload_length > 1) {
                    payload[payload_length - 1] = '\0';
                }

                strncat(payload, " ]", sizeof(payload) - payload_length);

                ESP_LOGI(TAG, "Complete Payload: %s", payload);

                esp_mqtt_client_publish(mqtt_client, "your_mqtt_topic", payload, 0, 1, 0);
                // Print all values in the chunk
//                for (int i = 0; i < CHUNK_SIZE; i++) {
//                    ESP_LOGI(TAG, "Value[%d]: %hhu", i, chunk[i]);
//                }
            } else {
                // Release mutex if there's not enough data for a complete set
                xSemaphoreGive(buffer_mutex);
            }
        }

        vTaskDelay(pdMS_TO_TICKS(1000));  // 1 second
    }
}

static void mqtt_task_test(void *pvParameter) {
    esp_mqtt5_connection_property_config_t connect_property = {
            .session_expiry_interval = 10,
            .maximum_packet_size = 1024,
            .receive_maximum = 65535,
            .topic_alias_maximum = 2,
            .request_resp_info = true,
            .request_problem_info = true,
            .will_delay_interval = 10,
            .payload_format_indicator = true,
            .message_expiry_interval = 10,
            .response_topic = "/test/response",
            .correlation_data = "123456",
            .correlation_data_len = 6,
    };

    esp_mqtt_client_config_t mqtt5_cfg = {
            .broker.address.uri = "mqtt://192.168.1.31:1337",
            .session.protocol_ver = MQTT_PROTOCOL_V_5,
            .network.disable_auto_reconnect = true,
            .credentials.username = "123",
            .credentials.authentication.password = "456",
            .session.last_will.topic = "/topic/will",
            .session.last_will.msg = "i will leave",
            .session.last_will.msg_len = 12,
            .session.last_will.qos = 1,
            .session.last_will.retain = true,
    };
}

void app_main(void) {

    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %" PRIu32 " bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    /* Initialize NVS partition */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        /* NVS partition was truncated
         * and needs to be erased */
        ESP_ERROR_CHECK(nvs_flash_erase());

        /* Retry nvs_flash_init */
        ESP_ERROR_CHECK(nvs_flash_init());
    }


    /* Initialize TCP/IP */
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    initialize_wifi();
    // Initialize MQTT
    initialize_mqtt();

    adc1_config_width(ADC_BITS);
    adc1_config_channel_atten(EXAMPLE_ADC1_CHAN0, ADC_ATTEN);

//    ESP_ERROR_CHECK(mqtt_app_start(MQTT_BROKER_URI, MQTT_TOPIC));

    buffer_mutex = xSemaphoreCreateMutex();
    if (buffer_mutex == NULL) {
        ESP_LOGE(TAG, "Failed to create buffer mutex");
        abort();  // Handle error as needed
    }
    xTaskCreatePinnedToCore(adc_task, "adc_task", 4096, NULL, 5, NULL, 0);
    xTaskCreatePinnedToCore(mqtt_task, "mqtt_task", 4096, NULL, 5, NULL, 1);
}


