
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <driver/adc.h>
#include <time.h>
#include <math.h>
#include <nvs_flash.h>
#include <mqtt5_client.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "soc/soc_caps.h"
#include "freertos/event_groups.h"
#include "esp_dsp.h"
#include "bufferconfig.h"
#include "adc.h"
#include "mqtt.h"
#include "fft.h"

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

#include "esp_adc/adc_oneshot.h"
#include "esp_adc/adc_cali.h"
#include "esp_adc/adc_cali_scheme.h"

#include "esp_wifi.h"


//#include "mqtt_client.h"


const static char *TAG = "SMART_WIRELESS_SENSOR";
/*---------------------------------------------------------------
        ADC General Macros
---------------------------------------------------------------*/
//ADC1 Channels
#if CONFIG_IDF_TARGET_ESP32
#define EXAMPLE_ADC1_CHAN0          ADC_CHANNEL_4
#else
#define EXAMPLE_ADC1_CHAN0          ADC_CHANNEL_2
#define EXAMPLE_ADC1_CHAN1          ADC_CHANNEL_3
#endif

#define ADC_BITS ADC_WIDTH_BIT_12
#define ADC_ATTEN ADC_ATTEN_DB_0

#if (SOC_ADC_PERIPH_NUM >= 2) && !CONFIG_IDF_TARGET_ESP32C3
/**
 * On ESP32C3, ADC2 is no longer supported, due to its HW limitation.
 * Search for errata on espressif website for more details.
 */
#endif

#define EXAMPLE_ADC_ATTEN           ADC_ATTEN_DB_11

#define MQTT_BROKER_URI "http://192.168.0.1"
#define MQTT_TOPIC "sensor/adc"

SemaphoreHandle_t buffer_mutex;
uint32_t buffer_head = 0;
#define MAX_BUFFER_SIZE 4096
#define NUMBER_OF_CHUNKS 16

BufferConfig buffer_config = {
        .size = MAX_BUFFER_SIZE,
        .num_chunks = NUMBER_OF_CHUNKS
};

typedef struct {
    SemaphoreHandle_t buffer_mutex;
    BufferConfig buffer_config;
}TaskParams_t;

void app_main(void) {

    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %" PRIu32 " bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    /* Initialize NVS partition */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        /* NVS partition was truncated
         * and needs to be erased */
        ESP_ERROR_CHECK(nvs_flash_erase());

        /* Retry nvs_flash_init */
        ESP_ERROR_CHECK(nvs_flash_init());
    }

    TaskParams_t *params = pvPortMalloc(sizeof(TaskParams_t)); // Use FreeRTOS malloc
    if (params == NULL) {
        // Handle memory allocation error…
    }

    ret = dsps_fft2r_init_fc32(NULL, CONFIG_DSP_MAX_FFT_SIZE);
    if (ret  != ESP_OK)
    {
        ESP_LOGE(TAG, "Not possible to initialize FFT. Error = %i", ret);
        return;
    }
    init_buffer_config(&buffer_config);
    print_buffer_config(&buffer_config);

    adc1_config_width(ADC_BITS);
    adc1_config_channel_atten(EXAMPLE_ADC1_CHAN0, ADC_ATTEN);

//    ESP_ERROR_CHECK(mqtt_app_start(MQTT_BROKER_URI, MQTT_TOPIC));

    buffer_mutex = xSemaphoreCreateMutex();
    if (buffer_mutex == NULL) {
        ESP_LOGE(TAG, "Failed to create buffer mutex");
        abort();  // Handle error as needed
    }

    params->buffer_mutex = buffer_mutex;
    params->buffer_config = buffer_config;

    xTaskCreatePinnedToCore(adc_task, "adc_task", 4096, params, 1, NULL, 0);
    xTaskCreatePinnedToCore(mqtt_task, "mqtt_task", 4096, params, 5, NULL, 1);

//    xTaskCreatePinnedToCore(fft_task, "fft_task", 4096, params, 5, NULL, 1);
}


//TODO: FINISH OFF UNIT TEST IMPLEMENTATION - BUFFER, PAYLOAD ECT
//TEST RUNNING AGAINST HARDWARE
//EXPERIMENT WITH SIGNAL CHARECTARISTICS AND POWER DERIVIATION