import numpy as np
import matplotlib.pyplot as plt

# Constants
PRIMARY_VOLTAGE = 240  # Primary voltage in volts (rms)
FREQUENCY = 60  # Frequency in Hz
TURNS_RATIO = 100  # Turns ratio (Np/Ns)
LOAD_RESISTANCE = 10  # Load resistance in ohms
TOASTER_POWER = 2000  # Toaster power in watts
SIMULATION_DURATION = 1  # Duration of the simulation in seconds


class CurrentTransformer:
    turns_ratio = 100  # Turns ratio (Np/Ns)

    def apply(self, primary_current):
        return primary_current / self.turns_ratio


class MicroController:
    sample_rate = 0.001 # 1/(60*24)
    samples_t = []
    samples_v = []

    def sample(self, voltage_waveform, t):
        for index, sample in enumerate(t):
            if sample % self.sample_rate == 0:
                self.samples_v.append(voltage_waveform[index])
        return self.samples_v


# Time settings
T_START = 0
T_END = SIMULATION_DURATION
T_STEP = 0.00001

CT = CurrentTransformer()
Controller = MicroController()
# Time vector
t = np.arange(T_START, T_END, T_STEP)

# Simulate primary voltage (sine wave)
primary_voltage_waveform = PRIMARY_VOLTAGE * np.sin(2 * np.pi * FREQUENCY * t)

# Initially, there is no load
load_power_waveform = np.zeros_like(t)

# Halfway through the simulation, the toaster is turned on
TOASTER_START_TIME = T_END / 2
TOASTER_END_TIME = T_END
load_power_waveform[int(TOASTER_START_TIME / T_STEP):] = TOASTER_POWER

# Simulate secondary voltage
secondary_voltage_waveform = CT.apply(primary_current=primary_voltage_waveform)

# Calculate secondary current based on the secondary voltage and load power
secondary_current_waveform = (secondary_voltage_waveform + np.sqrt(
    load_power_waveform * LOAD_RESISTANCE)) / LOAD_RESISTANCE
Sampled_values = Controller.sample(voltage_waveform=secondary_current_waveform, t=t)
# Calculate voltage across the load resistance
voltage_across_load = secondary_current_waveform * LOAD_RESISTANCE

# Live power line - > Current transformer output -> Signal conditioning -> MC -> Server

# Plot the results
plt.figure(figsize=(10, 8))
plt.subplot(5, 1, 1)
plt.plot(t, primary_voltage_waveform, label='Primary Voltage (V)')
plt.plot(t, secondary_voltage_waveform, label='Secondary Voltage (V)')
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
plt.legend()

plt.subplot(5, 1, 2)
plt.plot(t, secondary_current_waveform, label='Secondary Current (A)')
plt.xlabel('Time (s)')
plt.ylabel('Current (A)')
plt.legend()

plt.subplot(5, 1, 3)
plt.plot(t, load_power_waveform, label='Load Power (W)')
plt.xlabel('Time (s)')
plt.ylabel('Power (W)')
plt.legend()

plt.subplot(5, 1, 4)
plt.plot(t, voltage_across_load, label='Voltage Across Load (V)')
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
plt.legend()

plt.subplot(5, 1, 5)
plt.plot(Sampled_values, label='Voltage Across Load (V)')
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
plt.legend()

plt.tight_layout()
plt.show()
