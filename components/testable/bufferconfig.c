#include "bufferconfig.h"
#include <esp_log.h>

const static char *TAG = "BUFFERCONFIG.C";
#define MAX_BUFFER_SIZE 4096  // define the maximum size of the buffer

uint8_t bufferconfig_adc_values[MAX_BUFFER_SIZE];

void init_buffer_config(BufferConfig *buffer_config) {
    buffer_config->chunk_size = buffer_config->size / buffer_config->num_chunks;
    buffer_config->max_payload_size = 60 + 5 * buffer_config->chunk_size + 3;
}

void print_buffer_config(const BufferConfig *buffer_config) {
    ESP_LOGI(TAG, "Buffer Size: %zu", buffer_config->size);
    ESP_LOGI(TAG, "Number of Chunks: %zu", buffer_config->num_chunks);
    ESP_LOGI(TAG, "Chunk Size: %zu", buffer_config->chunk_size);
    ESP_LOGI(TAG, "Max Payload Size: %zu", buffer_config->max_payload_size);
}