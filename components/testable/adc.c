//
// Created by ven on 03/01/24.
//

#ifndef SMART_WIRELESS_SENSOR_ADC_H
#define SMART_WIRELESS_SENSOR_ADC_H

#endif //SMART_WIRELESS_SENSOR_ADC_H
// adc_tasks.c
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "adc.h"
#include <math.h>
#include "bufferconfig.h"
#include <esp_log.h>
const static char *TAG =  "(ADC.C)";

typedef struct {
    SemaphoreHandle_t buffer_mutex;
    BufferConfig buffer_config;
}TaskParams_t;


// Buffer and mutex are defined in mqtt_tasks.c
extern uint32_t buffer_head;
extern SemaphoreHandle_t buffer_mutex;
extern BufferConfig buffer_config;


static float t = 0.0f;

uint8_t generate_simulated_adc_value() {
    float amplitude = 127.0f;  // Adjust the amplitude as needed
    float frequency = 1.0f;  // Adjust the frequency as needed
    uint8_t simulated_adc_value = (uint8_t)(amplitude * sin(1 * M_PI * frequency * t)+ 127.0f);

    // Increment time variable for the next iteration
    t += 1.0f / 120.0f;  // Assuming a 120Hz sampling rate

    return simulated_adc_value;
}

void store_adc_value(uint8_t adc_value) {
    if (xSemaphoreTake(buffer_mutex, portMAX_DELAY)) {
        bufferconfig_adc_values[buffer_head] = adc_value;

        buffer_head = (buffer_head + 1) % buffer_config.size;
        if (buffer_head == buffer_config.size){
            buffer_head = 0;
        }
        xSemaphoreGive(buffer_mutex);
    }
}

void adc_task(void *pvParameter) {
    TaskParams_t *params = (TaskParams_t *)pvParameter;
    buffer_mutex = params->buffer_mutex;
    buffer_config = params->buffer_config;


    print_buffer_config(&buffer_config);
    ESP_LOGI(TAG, "Thread started");

    while (1) {
        // unsigned char adc_value = adc1_get_raw(EXAMPLE_ADC1_CHAN0); ## HARDWARE EQUIV
        uint8_t adc_value = generate_simulated_adc_value();
        store_adc_value(adc_value);
//        ESP_LOGI(TAG, "ADC Value: %d, Buffer Head: %lu", adc_value, buffer_head);


        vTaskDelay(pdMS_TO_TICKS(0.02));
    }
}