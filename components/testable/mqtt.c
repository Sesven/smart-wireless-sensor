// mqtt_tasks.c
#include "mqtt.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
//#include "constants.h"
#include "bufferconfig.h"
#include "analytics.h"
#include <string.h>
#include <stdio.h>
#include <esp_log.h>

//extern BufferConfig buffer_config;
extern SemaphoreHandle_t buffer_mutex;
extern uint32_t buffer_head;
extern BufferConfig buffer_config;

const static char *TAG = "(MQTT.C)";

typedef struct {
    SemaphoreHandle_t buffer_mutex;
    BufferConfig buffer_config;
} TaskParams_t;

void process_chunk() {
    unsigned char buffer_chunk[buffer_config.chunk_size];
    for (int i = 0; i < buffer_config.chunk_size; i++) {
        buffer_chunk[i] = bufferconfig_adc_values[i];
    }

    buffer_head = buffer_head - buffer_config.chunk_size;
    xSemaphoreGive(buffer_mutex);

    char payload[buffer_config.max_payload_size];
    size_t payload_size = sizeof(payload);
    create_payload(buffer_chunk, payload, payload_size);
//    ESP_LOGI(TAG, "Complete Payload: %s", payload);
    double root;
    root = rms(buffer_chunk, buffer_config.chunk_size);
    ESP_LOGI(TAG, "RMS VALUE IS : %f", root);

    //esp_mqtt_client_publish(mqtt_client, "your_mqtt_topic", payload, 0, 1, 0);
}
/**
 * Function Name: create_payload
 * Description: This function generates a stringified JSON payload. The payload includes
 *              a timestamp, data buffer length and data buffer content. It adds a series
 *              of unsigned chars to the string payload from a given chunk data buffer.
 *
 * @param chunk: Pointer to an array of unsigned chars. The content of these chars will be
 *               added to the payload.
 * @param payload: Pointer to a char buffer where the resulting JSON payload string will be
 *                 stored. This buffer must be allocated before calling this function. The
 *                 size of this buffer should be at least the total number of chars to be
 *                 printed plus one for the null-terminating char.
 * @param payload_size: The size of `payload` buffer. It should be equal to the maximum number
 *                      of chars that can be printed to `payload`, including the
 *                      null-terminating char.
 *
 * @return payload: A pointer to the start of the payload string.
 *
 * Note: The caller function is responsible for freeing the memory of `payload` to prevent
 *       memory leaks.
 *
 *Note: The function expects that buffer_config object is defined and accessible within the
 *      scope of this function.
*/
char *create_payload(unsigned char *chunk, char *payload, size_t payload_size) {

    time_t now;
    time(&now);

    int payload_length = snprintf(payload, payload_size, "{ \"timestamp\": %lld, "
                                                         "\"datalen\": %u "
                                                         "\"data\": [",
                                                         now, payload_size);

    for (int i = 0; i < buffer_config.chunk_size; i++) {
        payload_length += snprintf(payload + payload_length, payload_size - payload_length, " %u,", chunk[i]);
    }

    payload[payload_length - 1] = ']';
    payload[payload_length] = '\0';
    strncat(payload, " ]", payload_size - payload_length);
    return payload;
}

void mqtt_task(void *pvParameter) {
    TaskParams_t *params = (TaskParams_t *) pvParameter;
    buffer_mutex = params->buffer_mutex;
    buffer_config = params->buffer_config;

    ESP_LOGI(TAG, "Thread started");
    while (1) {
        if (xSemaphoreTake(buffer_mutex, portMAX_DELAY)) {
            if (buffer_config.chunk_size <= buffer_head) {
                process_chunk();
            } else {
                xSemaphoreGive(buffer_mutex);
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));  // 1 second
    }
}