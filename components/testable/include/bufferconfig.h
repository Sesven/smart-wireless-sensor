#ifndef BUFFERCONFIG_H
#define BUFFERCONFIG_H

#include <stddef.h>
#include <stdint.h>


typedef struct BufferConfig {
    size_t size;  // Size of the buffer
    size_t num_chunks;  // Number of chunks in a set
    size_t chunk_size;  // Size of a single chunk
    size_t max_payload_size;
} BufferConfig;


extern uint8_t bufferconfig_adc_values[];

void init_buffer_config(BufferConfig *);
void print_buffer_config(const BufferConfig* buffer_config);
#endif // BUFFERCONFIG_H