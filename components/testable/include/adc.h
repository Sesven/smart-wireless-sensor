//
// Created by ven on 03/01/24.
//

#ifndef ADC_TASKS_H
#define ADC_TASKS_H

#include <stdint.h>

void adc_task(void *pvParameter);
void store_adc_value(uint8_t adc_value);
uint8_t generate_simulated_adc_value();

#endif // ADC_TASKS_H