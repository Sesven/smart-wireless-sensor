//
// Created by ven on 03/01/24.
//
// mqtt_tasks.h
#ifndef MQTT_TASKS_H
#define MQTT_TASKS_H

#include "bufferconfig.h"
#include <time.h>

void mqtt_task(void *pvParameter);
void process_chunk();
char * create_payload (unsigned char *chunk, char* payload, size_t payload_size);

#endif // MQTT_TASKS_H
