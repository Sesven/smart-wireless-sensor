#ifndef FFT_H
#define FFT_H

void fft_task(void *pvParameter);
void fft_analyse();

#endif // FFT_H