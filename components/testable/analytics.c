#include <stdio.h>
#include <math.h>

double rms(unsigned char *v, size_t n)
{
    int i;
    double sum = 0.0;
    for(i = 0; i < n; i++)
        sum += v[i] * v[i];
    return sqrt(sum / n);
}
