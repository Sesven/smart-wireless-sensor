
#include <limits.h>
#include "unity.h"
#include "adc.h"
#include "bufferconfig.h"

#define MAX_BUFFER_SIZE 4096
uint8_t adc_values[MAX_BUFFER_SIZE];

TEST_CASE("Simulated adc value is valid", "[adc]")
{
    uint8_t value = generate_simulated_adc_value();
    TEST_ASSERT_UINT8_WITHIN(0, 0, value);
}


TEST_CASE("Values are stored correctly in ADC buffer", "[adc]") {

    BufferConfig buffer_config = {
            .size = MAX_BUFFER_SIZE,
            .num_chunks = 4

    };
    init_buffer_config(&buffer_config);

    for (uint8_t i = 0; i < buffer_config.size; i++) {
        store_adc_value(i);

        assert(adc_values[i] == i);
    }
}