#include "unity.h"
#include "bufferconfig.h"

TEST_CASE("Buffer initialisation is valid", "[buffer]") {
    BufferConfig buffer_config;
    buffer_config.size = 500;
    buffer_config.num_chunks = 10;

    init_buffer_config(&buffer_config);

    assert(buffer_config.chunk_size == 50);
    assert(buffer_config.max_payload_size == 313);
}