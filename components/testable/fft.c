#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include <math.h>
#include "bufferconfig.h"
#include "esp_dsp.h"

#include <esp_log.h>
const static char *TAG =  "(FFT.C)";

typedef struct {
    SemaphoreHandle_t buffer_mutex;
    BufferConfig buffer_config;
}TaskParams_t;


// Buffer and mutex are defined in mqtt_tasks.c
extern uint32_t buffer_head;
extern SemaphoreHandle_t buffer_mutex;
extern BufferConfig buffer_config;

#define N_SAMPLES 1200
int N = N_SAMPLES;

__attribute__((aligned(16)))
float x1[N_SAMPLES];

__attribute__((aligned(16)))
float wind[N_SAMPLES];

__attribute__((aligned(16)))
float y_cf[N_SAMPLES*2];

void fft_analyse() {
    ESP_LOGI(TAG, "Beginning Analysis");

    unsigned char buffer_chunk[N_SAMPLES];
    for (int i = 0; i < N; i++) {
        buffer_chunk[i] = bufferconfig_adc_values[i];
    }
    buffer_head = buffer_head - N_SAMPLES;
    xSemaphoreGive(buffer_mutex);
    for (int i = 0; i < N_SAMPLES; i++) {
        x1[i] = (float)buffer_chunk[i];
    }

//    dsps_tone_gen_f32(x1, N, 255.0, 0.008,  0);

    dsps_wind_hann_f32(wind, N);

    for (int i=0 ; i< N_SAMPLES ; i++) {
        y_cf[i*2 + 0] = x1[i] * wind[i];
    }

    // FFT
    unsigned int start_b = dsp_get_cpu_cycle_count();
    dsps_fft2r_fc32(y_cf, N);
    unsigned int end_b = dsp_get_cpu_cycle_count();

    dsps_bit_rev_fc32(y_cf, N);
    dsps_cplx2reC_fc32(y_cf, N);

    for (int i = 0 ; i < N/2 ; i++) {
        y_cf[i] = 10 * log10f((y_cf[i * 2 + 0] * y_cf[i * 2 + 0] + y_cf[i * 2 + 1] * y_cf[i * 2 + 1])/N);
    }

    ESP_LOGW(TAG, "Signal x1");
    dsps_view(y_cf, N/2, 64, 10,  -60, 40, '|');
    ESP_LOGI(TAG, "FFT for %i complex points take %i cycles", N, end_b - start_b);
}

void fft_task(void *pvParameter) {
    TaskParams_t *params = (TaskParams_t *)pvParameter;
    buffer_mutex = params->buffer_mutex;
    buffer_config = params->buffer_config;


    print_buffer_config(&buffer_config);

    ESP_LOGI(TAG, "Thread started");

    while (1) {
        // unsigned char adc_value = adc1_get_raw(EXAMPLE_ADC1_CHAN0); ## HARDWARE EQUIV

        if (xSemaphoreTake(buffer_mutex, portMAX_DELAY)) {
            if (N_SAMPLES <= buffer_head) {
                fft_analyse();
            } else {
                xSemaphoreGive(buffer_mutex);
            }
        }
        vTaskDelay(pdMS_TO_TICKS(70));
    }
}