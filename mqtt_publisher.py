import paho.mqtt.client as mqtt
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import time
import json
# # Generate an array of float values from 0 to 230
# start = 0
# end = 230
# num_values = 100  # You can change this to the desired number of values
# values = np.linspace(start, end, num_values)
#
# print(values)
import math
import random

client_type = "ESP32"
BUFFER_CAPACITY = 256
buffer = []

class SineWaveGenerator:
    def __init__(self, frequency=1.0, amplitude=1.0):
        self.frequency = frequency
        self.amplitude = amplitude

    def probe(self):
        current_time = time.time()  # get current system time, in seconds
        value = (self.amplitude * np.sin(2 * np.pi * self.frequency * current_time)) + self.amplitude
        return int(value)

    def change_amplitude(self, new_amplitude):
        self.amplitude = new_amplitude

class RMSComputer:
    def __init__(self, window_size=400):
        self.window = [0] * window_size
        self.index = 0
        self.last_rms_val = 0

    def calc_rms(self, value):
        self.window[self.index] = value ** 2
        self.index = (self.index + 1) % len(self.window)
        self.last_rms_val = round(math.sqrt(sum(self.window) / len(self.window)),1)
        return self.last_rms_val

class FFTComputer:
    def __init__(self, window_size=100, sample_rate=0.1):
        self.window = [0] * window_size
        self.index = 0
        self.sample_rate = sample_rate

    def add_value(self, value):
        self.window[self.index] = value
        self.index = (self.index + 1) % len(self.window)

    def calculate_fft(self):
        data = np.array(self.window)
        fft_output = np.fft.rfft(data)
        magnitudes = np.abs(fft_output)
        frequencies = np.linspace(0, self.sample_rate / 2, len(magnitudes))
        return frequencies, magnitudes
def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {rc}")
    client.subscribe("test_topic")
    client.subscribe("rms")
    client.subscribe("power")

def on_message(client, userdata, msg):
    print(f"Received message '{msg.payload.decode()}' on topic '{msg.topic}'")

def process_payload(chunk, payload_size, now):
    """Convert C code data to Python Dict"""
    # Create the base of the payload
    payload = {
        "timestamp": now,
        "datalen": payload_size,
        "data": []
    }

    # Fill in the array
    for i in range(len(chunk)):
        payload["data"].append(chunk[i])

    return payload


def process_payload_single_var(chunk, payload_size, now):
    """Convert C code data to Python Dict"""
    # Create the base of the payload
    payload = {
        "timestamp": now,
        "datalen": payload_size,
        "data": [chunk]
    }

    return payload

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

broker = "localhost"  # local address
port = 1883

client.connect(broker, port, 60)
generator = SineWaveGenerator(frequency=60.0, amplitude=127)
rms_computer = RMSComputer(window_size=1200)


SAMPLE_RATE = 0.000833# 1 / 120Hz = 0.00833s
fft_computer = FFTComputer(window_size=1600, sample_rate=240)

AMPLITUDE_HIGH = 127
AMPLITUDE_LOW = 10
amplitude_state = 0  #
power = 0
try:
    client.loop_start()
    while True:
        sinewave_value = generator.probe()
        buffer.append(sinewave_value)
        rms_computer.calc_rms(sinewave_value)

        # message = json.dumps({'sinewave_value': sinewave_value})
        # rms_value = rms_computer.calc_rms(sinewave_value)
        # fft_computer.add_value(sinewave_value)
        # freq, magnitudes = fft_computer.calculate_fft()
        # If buffer is at capacity, send the data
        if len(buffer) == BUFFER_CAPACITY:
            now = int(time.time())

            # Create payload
            payload = process_payload(buffer, BUFFER_CAPACITY, now)

            # Send the data
            client.publish("test_topic", json.dumps(payload))

            # Clear the buffer
            buffer.clear()


        # message = json.dumps({'sinewave_value': sinewave_value, 'RMS_value':rms_value})
        # print(rms_value)
        # client.publish("test_topic", message)
        time.sleep(SAMPLE_RATE)
        current_time = time.time()

        if abs(current_time % random.randint(10,60) - 0) < 0.0008:  # If it has been approximately 12 seconds
            generator.change_amplitude(random.randint(10,127))

        if abs(current_time % 3 - 0) < 0.0008:  # If it has been approximately 12 seconds
            now = int(time.time())
            payload = process_payload_single_var(rms_computer.last_rms_val, 1, now)
            client.publish("rms", json.dumps(payload))

            rms_current = (rms_computer.last_rms_val / 181) * 15
            power = power + (rms_current * 230)

            payload = process_payload_single_var(power, 1, now)
            client.publish("power", json.dumps(payload))
            # plt.figure(figsize=(14, 7))
            # plt.clf()
            # plt.plot(freq, magnitudes)
            # plt.title("FFT Analysis")
            # plt.xlabel('Frequency')
            # plt.ylabel('Magnitude')
            # plt.grid(True)
            # plt.savefig('fft_chart.png')

        # client.publish("test_topic", dt_string + " Client: " + client_type + "Sensordata" + values)
except KeyboardInterrupt:
    client.disconnect()

# What type of messages will I want to publish and receive?
# Sensor data
# Device info - Diagnostics -
#                           - Firmware info
#                           - Log data?
#             - Service mode - Battery levels
#                            - Network info
