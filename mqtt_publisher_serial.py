import paho.mqtt.client as mqtt
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import time
import json

import math
import random
import serial
import ast

client_type = "ESP32"
BUFFER_CAPACITY = 256
buffer = []

import re
import json

def get_rms_value(text: str) -> float:
    pattern = r"RMS VALUE IS : (\d+.\d+)"
    match = re.search(pattern, text)
    if match:
        return float(match.group(1))
    else:
        return None


def get_payload_data(text):
    # Load the json string
    try:
        # Find data in the form of [ ... ]
        pattern = r'"data": \[(.*?)\]'
        match = re.findall(pattern, text)

        if match:
            # The last match should be the data content
            data_string = "[" + match[-1] + "]"
            data_list = ast.literal_eval(data_string)

            return data_list
        else:
            return None
    except Exception as e:
        print(e)
        pass


ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)

def on_message(client, userdata, msg):
    print(f"Received message '{msg.payload.decode()}' on topic '{msg.topic}'")

def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {rc}")
    client.subscribe("test_topic")
    client.subscribe("rms")
    client.subscribe("power")

def process_payload(chunk, payload_size, now):
    """Convert C code data to Python Dict"""
    # Create the base of the payload
    payload = {
        "timestamp": now,
        "datalen": payload_size,
        "data": []
    }

    # Fill in the array
    for i in range(len(chunk)):
        payload["data"].append(chunk[i])

    return payload


def process_payload_single_var(chunk, payload_size, now):
    """Convert C code data to Python Dict"""
    # Create the base of the payload
    payload = {
        "timestamp": now,
        "datalen": payload_size,
        "data": [chunk]
    }

    return payload

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
broker = "localhost"  # local address
port = 1883

client.connect(broker, port, 60)

try:
    client.loop_start()
    while True:
        current_time = time.time()
        data = ser.readline().decode('utf-8')  # read data from serial port
        rms_value = get_rms_value(data)
        if rms_value:
            payload_data = process_payload_single_var(rms_value, 1, current_time)
            client.publish("rms", json.dumps(payload_data))

        payload_data = get_payload_data(data)
        if payload_data:
            payload_data = process_payload(payload_data, len(payload_data), current_time)
            client.publish("test_topic", json.dumps(payload_data))

except KeyboardInterrupt:
    client.disconnect()
# What type of messages will I want to publish and receive?
# Sensor data
# Device info - Diagnostics -
#                           - Firmware info
#                           - Log data?
#             - Service mode - Battery levels
#                            - Network info
