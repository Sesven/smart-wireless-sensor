import numpy as np
import matplotlib.pyplot as plt


class Resistor:
    def __init__(self, resistance):
        self.resistance = resistance

    def get_current(self, voltage):
        return voltage / self.resistance


class StepDownTransformer:
    def __init__(self, turns_ratio):
        self.turns_ratio = turns_ratio  # Ns/Np

    def step_down_voltage(self, voltage):
        return voltage * self.turns_ratio  # Vs = Vp * (Ns/Np)


class MicroController:
    def __init__(self, sampling_rate, transformer, resistor, bits):
        self.sampling_rate = sampling_rate
        self.transformer = transformer
        self.resistor = resistor
        self.max_value = 2 ** bits - 1

    def sample(self, time):
        times = []
        voltages = []
        currents = []
        for t in np.linspace(0.0, time, num=int(time * self.sampling_rate)):
            voltage = 230 * (np.sin(2 * np.pi * 60 * t) + 1)
            voltage_transformed = self.transformer.step_down_voltage(voltage)

            current = self.resistor.get_current(voltage_transformed)

            voltage_quant = np.round(voltage_transformed / 460 * self.max_value).clip(0, self.max_value)
            current_quant = np.round(current / (460 / self.transformer.turns_ratio) * self.max_value).clip(0,
                                                                                                           self.max_value)

            times.append(t)
            voltages.append(voltage_quant)
            currents.append(current_quant)

        bytes_per_second = self.calculate_bytes_per_second()
        print(f"Sampling Rate: {self.sampling_rate} samples/second, Bit Depth: {len(bin(self.max_value)) - 2}")
        print(f"Bytes per second: {bytes_per_second} bytes/second")
        return times, voltages, currents

    def calculate_bytes_per_second(self):
        return self.sampling_rate * (self.max_value.bit_length() / 8)


transformer = StepDownTransformer(turns_ratio=0.5)
resistor = Resistor(resistance=330)  # in ohms

microController_char = MicroController(sampling_rate=500, transformer=transformer, resistor=resistor, bits=8)
microController_int = MicroController(sampling_rate=500, transformer=transformer, resistor=resistor, bits=16)

times_char, voltages_char, currents_char = microController_char.sample(time=0.1)
times_int, voltages_int, currents_int = microController_int.sample(time=0.1)

plt.figure(figsize=(12, 8))

plt.subplot(2, 2, 1)
plt.plot(times_char, voltages_char)
plt.title('Voltage (8 bits)')
plt.ylabel('Quantized Voltage')

plt.subplot(2, 2, 2)
plt.plot(times_int, voltages_int)
plt.title('Voltage (16 bits)')

plt.subplot(2, 2, 3)
plt.plot(times_char, currents_char)
plt.title('Current (8 bits)')
plt.ylabel('Quantized Current')
plt.xlabel('Time (s)')

plt.subplot(2, 2, 4)
plt.plot(times_int, currents_int)
plt.title('Current (16 bits)')
plt.xlabel('Time (s)')

plt.tight_layout()
plt.show()
