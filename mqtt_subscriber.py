import paho.mqtt.client as mqtt

# Define the callback for when the client connects to the broker
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT broker")
        client.subscribe("test_topic")  # Subscribe to the desired MQTT topic
    else:
        print(f"Connection failed with code {rc}")

# Define the callback for when a message is received from the broker
def on_message(client, userdata, msg):
    print(f"Received message on topic '{msg.topic}': {msg.payload.decode()}")

# Create an MQTT client
client = mqtt.Client()

# Set up the callbacks
client.on_connect = on_connect
client.on_message = on_message

# Connect to the MQTT broker
broker = "localhost"  # Change this to the IP address or hostname of your MQTT broker
port = 1883  # Change this to the MQTT broker's port
client.connect(broker, port, 60)

# Start the client's loop to listen for incoming messages
client.loop_forever()
